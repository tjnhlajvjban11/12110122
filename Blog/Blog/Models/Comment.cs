﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        [Required]
        public int ID { set; get; }
        [Required]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Seconds;
            }
        }

        public int PostID { set; get; }
        public virtual Post Posts { set; get; }

    }
}